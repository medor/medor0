/**
 * This file is part of HTML2print.
 *
 * HTML2print is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with HTML2print.  If not, see <http://www.gnu.org/licenses/>.
 */


;(function(undefined) {

    function init(data) {
        $.ajaxSetup ({
            headers: data
        });

        var Model = Backbone.Model.extend({
            urlRoot : 'https://old.medor.coop/fr/api/article-membership/',
            url: function() {
                var original_url = Backbone.Model.prototype.url.call( this );
                var parsed_url = original_url + ( original_url.charAt( original_url.length - 1 ) == '/' ? '' : '/' );

                return parsed_url;
            }
        });


        var tpl = window.tpl || `
            <header>
                <% if (article.rubric.title ) { %>
                <p class="rubric-title"><%= article.rubric.title %></p>
                <% } %>
                <% if (article.rubric.subtitle ) { %>
                <p class="rubric-subtitle"><%= article.rubric.subtitle %></p>
                <% } %>
                <h1 class="headline"><%= article.title %></h1>
                <% if (article.subtitle ) { %>
                <h2 class="subhead"><%= article.subtitle %></h2>
                <% } %>
                <p class="header-byline"><%= article.authors %></p>

                <div class="lead">
                <%= article.lead %>
                </div>
            </header>

            <div class="body"><%= article.body %></div>
        `;

        var View = Backbone.View.extend({
            el: 'article',
            template: _.template(tpl),
            initialize: function() {
                this.listenTo(this.model, 'sync', this.render);
            },
            render: function() {
                this.$el.html(this.template(this.model.attributes));

                var customEvent = new Event("OnStoryReady");
                window.document.dispatchEvent(customEvent);

                return this;
            }
        });

        var CSSView = Backbone.View.extend({
            tagName: 'style',
            template: _.template('body { counter-reset: folio <% print(folio - 1) %>; } html:not(.facing) .sheet:nth-child(even) .main:before, html.facing .sheet:nth-child(odd) .main:before { content: "<% if (article.rubric.type == "enquete" ) { %>Enquête — <% } %><%= article.title %>"; }'),
            initialize: function() {
                $('head').append(this.$el);
                this.listenTo(this.model, 'sync', this.render);
            },
            render: function() {
                this.$el.html(this.template(this.model.attributes));
                return this;
            }
        });

        $('article[data-id]').each(function() {
            var id = $(this).data("id");
            var model = new Model({id: id});
            var view = new View({model: model, el: this});
            var cssview = new CSSView({model: model});
            model.fetch();
        });
    }


    $.get("../../settings.json", init);


    if (! Modernizr.regions) {
        console.log('no support for css regions; loading the polyfill');
        var script = document.createElement('script');
        script.setAttribute('src', '../../vendors/css-regions-polyfill-custom-medor.js');
        document.getElementsByTagName('head')[0].appendChild(script);
    };
})();
