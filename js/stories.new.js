/**
 * This file is part of HTML2print.
 *
 * HTML2print is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with HTML2print.  If not, see <http://www.gnu.org/licenses/>.
 */


 ;(function(undefined) {
  if (!Object.entries)
    Object.entries = function( obj ){
      var ownProps = Object.keys( obj ),
        i = ownProps.length,
        resArray = new Array(i); // preallocate the Array

      while (i--)
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      return resArray;
    };

  const groupBy = key => array =>
    array.reduce((objectsByKeyValue, obj) => {
      const value = obj[key];
      objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
      return objectsByKeyValue;
    }, {});

  const groupByRoles = groupBy('role');
  const groupByLicence = groupBy('licence');

  const valid_txt_role = ["Textes", "Texte", "Enquête", "Textes et photos"];
  const valid_txt_role_footer = ["Textes", "Texte", "Enquête"];
  const valid_illu_role = ["Illustrations", "Illustration"];

  var fn_list = [];


  function naturalJoin(names) {
    let text = "";
    if (names.length > 1) {
      text += names.slice(0, names.length - 1).join(', ') + " et " + names.slice(-1);
    } else {
      text += names.join(', ')
    }

    return text
  }


  Vue.component('html-fragment', {
    functional: true,
    props: ['html'],
    render(h, ctx) {
      const nodes = new Vue({
        beforeCreate() { this.$createElement = h }, // not necessary, but cleaner imho
        template: `<div>${ctx.props.html}</div>`
      }).$mount()._vnode.children
      return nodes
    }
  })


  Vue.component('art', {
    props: {
      src: String,
      rubricSubtitle: {
        default: "",
        type: String
      },
      folio: {
        default: 1,
        type: Number
      },
      fn_list: []
    },
    data () {
      return {
        section: null,
        title: null,
        subtitle: null,
        authors: [],
        header_image: null,
        header_text: null,
        content: null,
        endnote: null,
        footnotes: null
      }
    },
    computed: {
      // a computed getter
      authorsByRolesAndLicences: function () {
        // Get all the roles from members
        // Use Set to remove duplicate data and convert Set to Array again
        // result = [ "Textes", "Illustrations" ]
        const roles = Array.from(new Set(this.authors.map(d => d.role)));

        // Mapping roles and filter all licenses from members based on selected role
        // result =
        // [
        //     { "role": "Textes", "licenses": [ "CC BY-NC-ND" ] },
        //     { "role": "Illustrations", "licenses": [ "Tous droits réservés" ] }
        // ]
        const rolesWithLicenses = roles.map(role => {
          return {
            "role": role,
            "licenses": Array.from(new Set(this.authors.filter(d => d.role === role ).map(d => d.licence)))
          };
        });

        // Mapping rolesWithLicenses and filter all members based on selected role and license
        // result =
        // [
        //     {
        //         "role":"Textes",
        //         "licenses":
        //         [
        //             {
        //                 "license":"CC BY-NC-ND",
        //                 "members":["Albert Trucmuche","Roberta Bidulle"]
        //             }
        //         ]
        //     },
        //     {
        //         "role":"Illustrations",
        //         "licenses":
        //         [
        //             {
        //                 "license":"Tous droits réservés",
        //                 "members":["Nina Machin"]
        //             }
        //         ]
        //     }
        // ]
        const res = rolesWithLicenses.map(role => {
          const licensesWithMembers = role.licenses.map(license => {
            return {
              "license": license,
              "members":  this.authors.filter(member => member.licence === license && member.role === role.role).map(data => data.name)
            };
          });
          role['licenses'] = licensesWithMembers;
          return role;
        });

        return res
      },
      byline: function () {
        let res = this.authorsByRolesAndLicences;

        let allParts = []

        // sorting so that Texte credit is always on top
        res.sort(function(a, b) {
          if(valid_txt_role.includes(a.role)){
            return -1;
          } else {
            return 1;
          }
        });

        for (let i = 0, len = res.length; i < len; i++) {
          let dt_role = ""
          let dd_members = ""

          let role = res[i].role;
          let licenses = res[i].licenses;

          // role
          if (valid_txt_role.includes(role)) {
            dt_role = "<dt>Par</dt>";
          }
          else if (valid_illu_role.includes(role)) {
            dt_role = "<dt>Illustrations :</dt>";
          } else {
            dt_role = "<dt>" + role + " :</dt>";
          }

          // members
          dd_members = "<dd>";
          for (let j = 0, len = licenses.length; j < len; j++) {
            dd_members = " " + dd_members + naturalJoin(licenses[j].members);
          }
          dd_members = dd_members + "</dd>";

          // joining the two
          allParts.push(dt_role + dd_members);
        }

        // joining all credits
        return allParts.join("<br>");
      },
      footerByline: function () {
        let res = this.authorsByRolesAndLicences;

        let allParts = []

        // sorting so that Texte credit is always on top
        res.sort(function(a, b) {
          if(valid_txt_role_footer.includes(a.role)){
            return -1;
          } else {
            return 1;
          }
        });

        for (let i = 0, len = res.length; i < len; i++) {
          let dt_role = ""
          let dd_members = ""

          let role = res[i].role;
          let licenses = res[i].licenses;

          // role
          if (valid_txt_role_footer.includes(role)) {
            dt_role = "<dt></dt>";
          }
          else if (valid_illu_role.includes(role)) {
            if(role=="Illustration") {
              dt_role = "<dt>Illustration :</dt>";
            } else if(role=="Illustrations") {
              dt_role = "<dt>Illustrations :</dt>";
            }
          } else {
            dt_role = "<dt>" + role + " :</dt>";
          }

          // members
          dd_members = "<dd>";
          for (let j = 0, len = licenses.length; j < len; j++) {
            dd_members =  dd_members + " " + naturalJoin(licenses[j].members);
            if (licenses[j].license != "Tous droits réservés") {
              dd_members = dd_members + "<small>" + licenses[j].license + "</small>";
            }
          }
          dd_members = dd_members + "</dd>";

          // joining the two
          allParts.push(dt_role + dd_members);
        }

        return allParts.join("<br>");
      },
      runningTitle: function () {
        var title = "";
        if (this.section == "Enquête") {
          title = "Enquête — "
        }
        title += this.title;
        return title
      },
      folioCounter: function() {
        return this.folio - 1;
      },
      footnotes_ordered: function() {
        var ordered = [],
            footnotes = this.footnotes
        ;

        function find_by_uuid(uuid) {
          return function(entry) {
            return entry.uuid.startsWith(uuid);
          }
        }
        fn_list.forEach(function(item, index) {
          var fn = footnotes.find(find_by_uuid(item));
          if (fn) { ordered.push(fn); }
        });

        return ordered;
      }
    },
    methods: {
      get_fn_index : function(val) {
          var _id = val.substring(1,7);

          if (fn_list.indexOf(_id) == -1) {
            fn_list.push(_id);
          };

          return (fn_list.indexOf(_id) + 1);
      },
      process_fn_refs: function(value) {
        return value.replace(/footnote/g, 'sup').replace(/\[[a-f0-9]{6}\]/g, this.get_fn_index);
      }
    },
    template: `<article>
      <component is="style">
        body { counter-reset: folio {{ folioCounter }}; }

        html:not(.facing) .sheet:nth-child(even) .main:before,
        html.facing .sheet:nth-child(odd) .main:before { content: "{{ runningTitle }}"; }
      </component>

      <header>
        <p class="rubric-title" v-if="section" v-html="section"></p>
        <p class="rubric-subtitle" v-if="rubricSubtitle" v-html="rubricSubtitle"></p>
        <h1 class="headline" v-html="title"></h1>
        <h2 class="subhead" v-if="subtitle" v-html="subtitle"></h2>
        <dl class="header-byline" v-html="byline"></dl>
        <div class="lead" v-if="header_text" v-html="header_text"></div>
      </header>

      <div class="body">
        <figure v-if="header_image">
          <img :src="header_image.url" alt=""/>
          <figcaption></figcaption>
        </figure>

        <template v-for="block in content">
          <html-fragment :html="process_fn_refs(block.value)" v-if="block && block.type == 'richtext'"></html-fragment>

          <blockquote v-if="block && block.type == 'pullout'" class="pull-out">
            <div v-html="block.value.quote"></div>
            <footer v-if="block.value.signature" v-html="block.value.signature"></footer>
          </blockquote>

          <figure v-if="block && block.type == 'figure'">
            <img :src="block.value.image_url" alt=""/>
            <figcaption>
              <html-fragment :html="block.value.caption" v-if="block.value.caption"></html-fragment>
              <footer>
                <html-fragment :html="block.value.credits" v-if="block.value.credits"></html-fragment>
                <small v-if="block.value.licence" v-html="block.value.licence"></small>
              </footer>
            </figcaption>
          </figure>

          <template v-if="block && block.type == 'image_gallery'">
          <figure v-for="blockbis in block.value.images">
            <img :src="blockbis.image_url" alt=""/>
            <figcaption>
              <html-fragment :html="blockbis.text" v-if="blockbis.text"></html-fragment>
              <footer v-if="blockbis.credits" v-html="blockbis.credits"></footer>
            </figcaption>
          </figure>
          </template>

          <aside v-if="block && block.type == 'sidebar'" class="sidebar">
            <template v-for="blockbis in block.value">
              <html-fragment :html="process_fn_refs(blockbis.value)" v-if="blockbis && blockbis.type == 'richtext'"></html-fragment>

              <blockquote v-if="blockbis && blockbis.type == 'pullout'" class="pull-out">
                <div v-html="blockbis.value.quote"></div>
                <footer v-if="blockbis.value.signature" v-html="blockbis.value.signature"></footer>
              </blockquote>

              <figure v-if="blockbis && blockbis.type == 'figure'">
                <img :src="blockbis.value.image_url" alt=""/>
                <figcaption>
                  <html-fragment :html="blockbis.value.text" v-if="blockbis.value.text"></html-fragment>
                  <footer>
                    <html-fragment :html="blockbis.value.credits" v-if="blockbis.value.credits"></html-fragment>
                    <small v-if="blockbis.value.licence" v-html="blockbis.value.licence"></small>
                  </footer>
                </figcaption>
              </figure>

            </template>
          </aside>
        </template>
      </div>
      <footer>
        <dl class="footer-byline" v-html="footerByline"></dl>
        <div v-if="endnote" v-html="endnote" class="endnote"></div>
      </footer>

      <ol v-if="footnotes" class="footnotes">
        <li v-for="footnote in footnotes_ordered" v-html="footnote.content"></li>
      </ol>
    </article>`,
    mounted () {
      axios
        .get(this.src)
        .then(response => {
          this.section = response.data.section;
          this.title = response.data.title;
          this.subtitle = response.data.subtitle;
          this.header_image = response.data.header_image;
          this.header_text = response.data.header_text;
          // https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
          // this.header_text = response.data.header_text.replace(/\u00A0/gim, function(i) {
          //   return '&#'+i.charCodeAt(0)+';';
          // });
          this.authors = response.data.authors;
          this.content = response.data.content;
          this.endnote = response.data.endnote;
          this.footnotes = response.data.footnotes;
        })
    },
    updated: function () {
      var customEvent = new Event("OnStoryReady");
      window.document.dispatchEvent(customEvent);
    }
  })

  var request = new XMLHttpRequest();
  request.open('GET', '../../settings.json', true);

  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      // Success!
      var data = JSON.parse(this.response);
      axios.defaults.headers.common['Authorization'] = data['Authorization'];

      var app = new Vue({
        el: '#stories',
        preserveWhitespace: true,
      })
    } else {
      // We reached our target server, but it returned an error
    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
  };

  request.send();
})();
