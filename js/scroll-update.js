
// remember scroll position through reload using `r`
// NOTE: this script cannot be global (in index.html at root)
// because it cannot change the scroll position of the iframe

document.addEventListener("OnStoryReady", function() {
	// when the document has fully loaded we scroll back to the position

	let x = sessionStorage.getItem("scrollX");
	let y = sessionStorage.getItem("scrollY");
	window.scroll(x, y);
	// console.log("--> scrolling back to " + x + "-" + y);
});

document.addEventListener('keypress', function(event) {
	// press `r` to reload

	console.log(event.charCode)
  if(event.charCode == 114){
    window.location.reload();
  }
});
