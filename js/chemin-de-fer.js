/**
 * This file is part of HTML2print.
 *
 * HTML2print is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * HTML2print is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with HTML2print.  If not, see <http://www.gnu.org/licenses/>.
 */


;(function(undefined) {

    function init(data) {
        $.ajaxSetup ({
            headers: data
        });
https://medor.coop/fr/api/article-membership/?issue=13

        var Model = Backbone.Model.extend({
            urlRoot : 'https://medor.coop/fr/api/article-membership/',
            url: function() {
                var original_url = Backbone.Model.prototype.url.call( this );
                var parsed_url = original_url + ( original_url.charAt( original_url.length - 1 ) == '/' ? '' : '/' );

                return parsed_url;
            }
        });

        var Collection = Backbone.Collection.extend({
            url : 'https://medor.coop/fr/api/article-membership/',
            model: Model,
        });


        var View = Marionette.View.extend({
            tagName: 'article',
            template: _.template($("#hello-template").text())
            // template: _.template("<% for (var i=0; i < page_number; i++) { %><div class='page'><div class='title'><%- slug %><% if ((folio + i) % 2) %>odd<% } %><%- folio + i %></div></div><% } %>"),
        });

        var CollectionView = Marionette.CollectionView.extend({
            el: '.container',
            childView: View,
            initialize: function () {
                this.listenTo(this.collection, 'sync', this.render);
            },
        });


        var collection = new Collection();
        var view = new CollectionView({collection: collection});
        collection.fetch({data: $.param({issue: 13})});

        window.view = view;
    }


    $.get("../../settings.json", init);
})();
