

// UN TOOLKIT TRES PRATIQUE
// ============================================================================
// console.log("TOOLKIT READY");


function multiple_first_letters(n, m=0){
  // avoir les n premier glyphes en lettrine
  // pour les articles qui commencent par (guillemet + lettre) par exemple

  let e = document.querySelector(".body p:first-of-type")
  , html = e.innerHTML
  , len = html.length;

  let preHtml = e.innerHTML.substring(0, m)
  , firstLetters = e.innerHTML.substring(m, n+m)
  , postHtml = e.innerHTML.substring(n+m, len);

  let startTag = '<span class="first-letters">'
  , endTag = '</span>';

  e.innerHTML =  preHtml + startTag + firstLetters + endTag + postHtml;
}


function add_br_at(selector, i){
  // ajoute une br après le ieme espace
  // par defaut dans le headline

  let e_list = document.querySelectorAll(selector);
  for (var j = 0; j < e_list.length; j++) {
   let e_wordlist = e_list[j].innerHTML.split(" ");
   e_wordlist[i] = "<br/>" + e_wordlist[i];
   e_list[j].innerHTML = e_wordlist.join(" ");
 }
}

function replace(selector, toreplace, replacewith){
  // remplace une string par une autre dans l'HTML
  // de l'élément selectionné

  let e_list = document.querySelectorAll(selector);

  // pour remplacer dans tout les éléments selectionnés
  for (var i = 0; i < e_list.length; i++) {
    console.log(e_list[i].innerHTML);
    // pour remplacer toutes les occurences dans l'élément
    let regex = new RegExp(toreplace, "g");
    e_list[i].innerHTML = e_list[i].innerHTML.replace(regex, replacewith);
  }
}
// EXEMPLE:
// ou encore, avec une regex:
// replace("h2:not(.subhead)","Étape ([0-9]).","<span>Étape $1.</span>");

// met un span autour d'une partie pour la stylisé différement
function spanify(selector, tospanify){
  replace(selector, tospanify, "<span>"+tospanify+"</span>");
}

function hardcode(selector, text){
  $(selector).html(text);
}

function credits_portfolio(){
  // met uniquement lae photographe en crédit de page d'ouverture
  // (et uniquement lae auteur.trice en crédit de fin de texte)

  // seulement credit photo au début
  let header = $(".header-byline"),
  header_lines_roles = header.find('dt'),
  header_lines_members = header.find('dd'),
  header_search = "photo",
  new_header = "photographe not found";

  for (var i = 0; i < header_lines_roles.length; i++) {
    console.log(header_lines_roles[i]);
    let role = $(header_lines_roles[i]).html();
    if(role.toLowerCase().includes(header_search)){
      $(header_lines_roles[i]).html("Par ");
    } else{
      $(header_lines_roles[i]).remove();
      $(header_lines_members[i]).remove();
    }
  }
  header.find('br').remove();
}
