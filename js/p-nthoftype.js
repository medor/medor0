
document.addEventListener("OnStoryReady", function() {
  let paras = document.querySelectorAll('.body > p');
  console.log(paras.length);
  for (var i = 0; i < paras.length; i++) {
    let index = i+1;
    // Note: you should not attach event listener in a for loop
    // because the i variable is not scoped (it will end ups being the same
    // in every function), but using `let` in the loop scope it.
    paras[i].addEventListener("click", function() {
      let css = ".body > p:nth-of-type("+index+"){\n  color: midnightblue;\n  word-spacing: -0.025em;\n}";
      alert(css)
    });
  }
});
