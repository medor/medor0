#! /usr/bin/env python3

# written by Dorian Timmermans (Open Source Publishing) in 2021

# this program was specifically written for Médor, in the 24 edition
# you can use it with `make toc`
# it takes in input the toc.yaml file at the root
# and generate a 2 pages html structure at buid/toc.html, for the sommaire of Médor
# you can then copy and paste this html inside the layout/sommaire.html


import sys
import yaml
import math
from yaml.loader import SafeLoader
from string import Template
import unidecode

def indent(text, amount, ch=' '):
    padding = amount * ch
    return ''.join(padding+line for line in text.splitlines(True))

def titleToId(title):
    split = title.split()
    new = []
    for word in split:
        #remove non-alphanum
        word = (x for x in word if x.isalnum())
        word = "".join(word)
        #only keep ascii and put in lower
        word = unidecode.unidecode(word).lower()
        new.append(word)
    return "-".join(new)


#   TEMPLATES
# ==================================================

pages_tpl= """<div id="page-$number" class="sheet">
  <div class="page">
    <div class="container main sommaire">
        $content
    </div>
  </div>
</div>"""

header_tpl = """
<div class="cellule headline"
style="-webkit-grid-column-start: 1;
       -webkit-grid-column-end: span 6;
       -webkit-grid-row-start: 1;
       -webkit-grid-row-end: span 1;" >
  <h1>sommaire</h1>
  <div id="date">
    <p class="date">
        Médor $numero<br>
        $date
    </p>
  </div>
</div>"""

cell_tpl = """
<div id="$id" class="cellule $type"
style="-webkit-grid-column-start: $col_start;
       -webkit-grid-column-end: span $col_span;
       -webkit-grid-row-start: $row_start;
       -webkit-grid-row-end: span $row_span;" >
    <h1>$titre</h1>
    <p class="phrase">$phrase</p>
    <p class="pagination">p. $folio</p>
</div>"""

cellwcat_tpl = """
<div id="$id" class="cellule $type"
style="-webkit-grid-column-start: $col_start;
       -webkit-grid-column-end: span $col_span;
       -webkit-grid-row-start: $row_start;
       -webkit-grid-row-end: span $row_span;" >
    <h1>$titre</h1>
    <p class="phrase">$phrase</p>
    <p class="categorie">$categorie</p>
    <p class="pagination">p. $folio</p>
</div>"""


#   GENERATING HTML
# ==================================================

# open the html file to write in it
f_out = open(sys.argv[2], "w")
pages = [[],[]]

# open and parse the yaml file
with open(sys.argv[1]) as f:

    data = yaml.load(f, Loader=SafeLoader)
    # append the HEADER toc block
    pages[0].append(Template(header_tpl).substitute(data))
    # sort the list
    data["articles"] = sorted(data["articles"], key = lambda i: i["folio"])

    current_page = 0
    num = 0
    for entry in data["articles"]:

        # check if phrase
        if not 'phrase' in entry:
            entry['phrase'] = ''

        # go to next page
        if entry["folio"] >= 64 and current_page == 0:
            current_page = current_page + 1
            num = 0

        # place on the page
        entry["col_start"] = ((num * 3) % 9) + 1
        entry["col_span"] = 3
        entry["row_start"] = (math.floor(num / 3) * 3) + 2
        entry["row_span"] = 2 if entry["type"]=="rubric" else 3

        # fill html template
        entry["id"] = titleToId(entry["titre"])
        tpl = cellwcat_tpl if "categorie" in entry else cell_tpl
        cell_html = Template(tpl).substitute(entry)

        # append to correct page
        pages[current_page].append(cell_html)
        num = num + 1


# convert pages list of item to a string
pages[0] = Template(pages_tpl).substitute({'number': 1, 'content': indent("\n".join(pages[0]), 6)})
pages[1] = Template(pages_tpl).substitute({'number': 2, 'content': indent("\n".join(pages[1]), 6)})

# write the html
f_out.write("\n".join(pages))
