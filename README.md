# medor0

Maquette médor pour démarrer un nouveau numéro avec touts les layouts (lancée a partir du num 21).
Si des modification permanentes sont nécessaires après un numéro, c'est ici qu'il faut les reporter.

### Pre-requisites

Generate the dropdown list of content for HTML

    make json

### Using HTML2print

Starts the serveur

    make serve

Open [OSPkit](https://gitlab.constantvzw.org/osp/tools.ospkit):

    cd ~/src/tools.ospkit/src
    ./OSPKit

Visit the URL:

    http://localhost:8000


### NEWS in médor0

* une **cheatsheet.md** qui documente un panel de tricks (à compléter au fur et à mesure)
* un script qui retiens la position de scroll (`r` pour refresh)
* un script qui montre sur le quel `nth-of-type` paragraphe on à cliquer (pour `word-spacing` ou `indent`)
* un **toolkit.js** pour:
  * Ajouter des sauts de lignes (plus de balise dans le wagtail) (par exemple dans les titres)
  * Remplacer du texte (peut-être utiliser pour ajouter des span autour d'un mot)
  * Mettre plusieurs caractères d'affiler en lettrine
  * et autre...
* du **mixins.less** pour:
  * Couper et positionner une image sur une double qui se joint parfaitement
  * remettre la `n` image dans le flux principal
  * Rentrer/sortir les exergues de la colonne vide
  * cacher une exergues en gardant l'indentation du paragraphe suivant
  * reset le style des captions quand on les bouges indépendament
  * et autre...
* une commande **make toc** pour générer un template de sommaire par defaut sur base de `yaml`.

### TODO for better medor0

CSS
  * deux classes principale ("article", "rubric"), puis ("enquete","flamand","recit","entretien","portrait" hérite de "article"). Pour le moment "enquete" est le seul qui a vraiment un style stable.
  * revoir les lettrines (toutes différentes, certaines manquantes) -> global?
  * revoir les sup / sub (tous différents, certains manquants) -> global?
  * h3 / systèmes de question réponse pour les interviews/entretien en global (car peut être présent dans une enquête, un récit ou flamand)
  * faire les style par défaut:
    * recit
    * portrait
    * entretien
    * moment-flamand (quasi même que enquête pour le moment?)

BUGS TO CORRECT
  * enquete, flamand: le corps de texte ne va pas jusqu’à la fin de la colonne (on doit ajouter des négatives margins)
  * script pullout.js fonctionnel (correction des marges des exergues automatiques)
  * les titres des encadrées sur deux lignes décalent le texte
  * les questions h3 des entretiens décalent le texte
  * enquete, rubric, flamand: quand un h2 commence une colonne ça décale le texte

BONNUS
  * tester possibilitées *footnotes* en bas de page ou verticale ?
  * revoir *calculs de la grille*
  * layout/ et layout-template/ folders séparés -> CANT
  * "quotes" in exergue outside of the block (+ clear rules on when to do it)
  * augmenter la mixin de double pour avoir plusieur alignement
    (centrer / top / bottom / left / right)
  * (?) inter & exergue: padding haut/bas plutôt que margins pour voir
    les accents