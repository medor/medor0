# Médor / HTML2PRINT &mdash; CheatSheet

## Commencer un numéro

### Nouveau repo gitlab

1. créer un nouveau répo vide et **privé** sur <https://gitlab.constantvzw.org/medor/>
2. cloner le répo maquette <https://gitlab.constantvzw.org/medor/medor0>
3. copier-coller le contenu du medor0 dans le nouveau, `add` `commit` et `push`
4. copier-coller la clé privé `settings.json` (pour l'accès Wagtail) en local
5. vérifier que `settings.json` et `build/` est en `.gitignore` (le copier du dernier numéro)
6. changer le numéro dans `css/layout.lss` sous le format "Médor - [saison] [année]"
7. renomer le dossier `layout` en `layout.examples` et creer un nouveau dossier `layout` vide

### Nouveau numéro dans le Wagtail

Sur le CMS <http://medor.coop/admin>

1. Pages > Médor Web > Les magazines
2. ajouter une sous-page
3. ajouter les pilotes et une images
4. en cochant la saison et l'année, le nom du numéro se génère
5. créer une nouvelle collection pour les images (paramètres > collection)

### Ouvrir les articles dans l'OSPKit

Pour voir les articles dans l'OSPKit, il faut

1. Dans le dossier du repo du numéro faire `make json` pour générer l'index de ce qui se trouve dans le dossier `layout/`
2. Dans le dossier du repo du numéro faire `make serve` pour lancer un serveur local (nécéssaire pour compiler les fichiers `.less` par [less CSS](https://lesscss.org/))
3. lancer l'OSPKit en faisant `./OSPKit` dans son dossier
4. se rendre sur `http://localhost:8000`


## Commencer la mise en page d'un article

### Couler l'article

Couler l'article dans le Wagtail à <http://medor.coop/admin>, en créant une nouvelle sous-page du numéro.

### Layouts

Les articles se divisent en différents layouts prédéfinis.
Pour la plupart de ceux-ci, un dossier de template se trouve dans `layout.examples/`.

1. copier-coller un des dossier de `layout.examples/` à `layout/` et le renomer. **Notes**: on ne peut pas mettre d'espace dans le nom du dossier, sinon le `make json` ne passe pas
2. introduire l'id de l'article dans l'url à la fin de l'HTML
```HTML
<div id="stories">
	<!-- id = 780 -->
	<art id="flow-main" class="entretien" folio="1" src="https://medor.coop/api/v2/pages/780"></art>
</div>
```

La classe de l'élément `art` applique un style CSS prédéfinis à l'article.

<!-- ### Liste des layouts

Liste des dossiers de layout et leur classes correspondante:

* edito - ```class="edito"```
* enquete-long et enquete-short - ```class="enquete"```.
La long a une double page d'intro custom, tandis que la short a un seul page.
* entretien - ```class="entretien"```
*	moment-flamand - ```class="flamand"```
* portfolio - ```class="portfolio"```
* portrait - ```class="portrait"```
* recit - ```class="recit"```
* sommaire - ```class="sommaire"```
* rubric - ```class="rubric"```. Le layout par défaut concerne toutes les rubriques: Aire libre, Fulguro-Point, Trait belge, L'enquêteke, Le sexe de Médor, Étude du milieu. Et des layouts articuliers:
	* rubric-web - ```class="rubric web"```. Layout à part.
	* rubric-droit - ```class="rubric droit"```. Enlever titres principaux, intertitres centré, et signature de fin.
	* rubric-jouer - ```class="rubric"```. Double-page d'images.
	* rub-nombril - ```class="nombril"```. Classe à part des rubrique avec colonnes en walf.

[note sur la charte graphique de chaque template ?? + simple, double et 3/4] -->

### Sous-titre de têtière

Pour ajouter un sous-titre à côté de la têtière, notamment pour préciser les différentes rubriques, on utilise l'attribut `rubric-subtitle` de l'élément `art`.

Les différents sous-titres des rubriques se trouvent en commentaire dans le layout rubric.

```HTML
<div id="stories">
	<art id="flow-main" class="rubric"  rubric-subtitle="Quand Médor indague sur un dPositionner des éléments sur la grille
étail-farce du quotidien" folio="3" src="https://medor.coop/api/v2/pages/778"></art>
</div>
```

### Paramètres URL

La fin de l'url dans l'élément `art` peut se terminer avec `?draft=true`, si l'article est en brouillon dans le Wagtail.
```HTML
<art id="flow-main" class="entretien" folio="1" src="https://medor.coop/api/v2/pages/780/?draft=true"></art>
```
Pour éviter les chargements d'images trop long, on ajoute `?image_preview=true`
```HTML
<art id="flow-main" class="entretien" folio="1" src="https://medor.coop/api/v2/pages/780/?draft=true&image-preview=true"></art>
```
Note que cela ne réduit pas la résolution des images misent en `background`, seulement celle en qui coule via directement du Wagtail en `figure`.

### Faire commencer les articles à gauche ou à droite

La plupart des articles commencent sur une page de gauche, certain sur une page de droite.
La classe `facing` permet de commencer à gauche, il suffit de l'enlever pour commencer à droite.

```HTML
<html lang="fr" class="normal facing">
```

### Coulage particulier: Coulisses du web

Les différents sous-articles de coulisses du web sont séparé en `encadrés`, souvent 1 par page.
Chaque encadré commence par un `h4` qui sert de taxinomie et `h2` pour titre du sous-article.
Les intertitres sont des `h3`.
La signature de chaque encadré est un `bold` en fin d'encadré.
Les boites "mais aussi:..." sont des `exergues` placé dans leur encadrés correspondant, avec le "mais aussi" qui est un paragraphe en `bold`. Ces exergues peuvent être sortis du flow et placé séparement.
Les illustrations doivent être recherché sur le wagtail et insérée
* ou en début d'article hors des encadrés pour les utiliser en fonds comme d'habitude, c'est-à-dire avec un bloc et la classe .flow-fig1 etc.
* ou si on les veut à même le flow de l'article on les insére à leur places dans les encadrés (souvent entre le titre et le premier paragraphe), en utilisant la mixins fonction `.flow-figure()`

---

## Bugs importants

### Transparency bug

`RGB2CMYK`, le script qui convertit en cmyk pour l'impression, rasterize l'entièretée de la page en mauvaise qualitée quand il encontre de la transparence.

C'est-à-dire que:
* png transparent,
* `opacity:`
* `text-shadow:`
* `rgba()`
  
sont à éviter à tout pris!

Deux solutions:
1. sortir l'image en .svg si possible (pour un titre custom, par exemple). Les .svg ne peuvent pas être uploader sur le Wagtail, une solution est de les mettre en local dans le dossier de l'article et appeler le fond avec `background: url([nom-du-fichier].svg)`.
2. remplacer les parties transparentes par ce qui doit se trouver en dessous (couleur de fond, blanc papier, ou autre) pour avoir un .png sans transparence.

### Clipping bug

En utilisant CSS regions, certains accents ou descendantes de textes italic
sont clippés par leur boite contenante. La manière la plus simple de fixer le bug est
d'ajouter du padding et ensuite d'annuler le décalage avec une marge négative.

Le bug est documenté ici: [https://github.com/annulen/webkit/issues/278](https://github.com/annulen/webkit/issues/278)
```CSS
.enquete .lead{
    margin-left: -10pt;
    padding-left: 10pt;
}
```

Parfois l'effet ne fonctionne pas car le bloc n'est pas positionné sur la grille de manière a prendre en compte les margins.
Il est préférable de les positionner en **top, bottom, left, right** dans ce cas.


### pt approximation bug

[to complete]


---

## Médor CSS: classes, protocoles et astuces

### La grille

Le positionnement des éléments sur la grille se fait avec une série de classes accompagnées de coordonnées.

La position par rapport au top-left corner en **x** et **y** ainsi que la **width** et **height** du bloc.
```HTML
<div class="bloc x1 y6 w4 h5 flow-main"></div>
```

La position par rapport au 4 bords de la grille, **top**, **bottom**, **left**, **right**.
```HTML
<div class="bloc t5 b-1 l-1 r-1 flow-fig1"></div>
```
On peut utiliser ```-1``` comme coordonnée, ce qui fait déborder le bloc de la grille jusqu'au bord de la page, notamment pour mettre des images en pleine page.

La classe `.walf` permet de donner a un bloc une largeur de la moitié de la grille.
```HTML
<div class="bloc t0 l0 walf b0 flow-main"></div>
<div class="bloc t0 r0 walf b0 flow-main"></div>
```

La classe `.walf-bleed` permet de donner a un bloc une largeur de la moitié de la grille plus les fonds perdus. Elle doit donc s'utiliser avec `l-1` ou `r-1`.
```HTML
<div class="bloc t0 l-1 walf-bleed b0 flow-main"></div>
<div class="bloc t0 r-1 walf-bleed b0 flow-main"></div>
```

On peut ajouter des `margin` positives ou négatives, directement en inline css sur ces éléments pour les ajuster plus finement que la grille (souvent en restant dans des multiples de `12pt`).

Note que ces `margin` ne prennent effet que sur des côtés sur lesquelles le bloc repose en `position: absolute`.
Par exemple un bloc en `x1 y6 w4 h5` ne sera pas affecter par `margin-right: -24pt`, car son côté droit est défini par une largeur `w4` et non par une position. On peut passer les côtés sur lesquelles on veut que les marges s'appliquent en absolue avec les classes `t` `b` `l` `r`.


#### Variables de construction de la page et de la grille

Ces variables sont calculées les unes en fonction des autres et servent à construire la page et la grille.
Elle peuvent être récupérées ailleurs.

**la page**
* ```@page-width: 482pt;```
* ```@page-height: 652pt;```

**la grille**
* ```@col-gutter: 9pt;```
* ```@row-gutter: 12pt;```
* ```@col-width: 38.66666667pt```
* ```@row-height: 36pt;```
* ```@col-gutter-width: 47.66666667pt```
* ```@row-gutter-height: 48;```

**une ligne**
* ```@line-height: 12pt;```


### Entités HTML: contrôle des saut de lignes et césures

Le contrôle des saut de lignes et césures ce fait en insérerant des entités HTML directement dans le Wagtail, tels que non-breakable-space, shy ou autre.

Pour empêcher de casser entre deux mots (ex: "21 avril"), on ajoute un  `&nbsp;` (unicode: `00a0`).

Pour forcer une césure à être a un certain endroit, on ajoute le caractère invisible `&shy;` (unicode: `00ad`).

Pour empêcher une suite de caractère d'avoir un line-break ou une césure, on utilise la classe `keep` via le Wagtail.

```HTML
<span class="keep">un mot ici</span>
```

L'ajout des entités HTML (comme `&nbsp;`) ne fonctionne en les écrivant en HTML car certain scripts vont vouloir corriger la mise en forme typographique et ajouter des espaces fins avant les points-virgules, ce qui va briser le `&nbsp;`. 

La meilleur façon d'ajouter des caractères spéciaux est de les entrer directement dans le Wagtail en utilisant `Ctrl+Shift+u` puis le code du caractère, par exemple `00a0` pour le non-breakable-space, puis enter.

Liste des codes des entités HTML [https://unicode-table.com/en/html-entities/](https://unicode-table.com/en/html-entities/).


### Notes pour récupérer des éléments en CSS

Il est préférable de préciser la classe du layout avant la classe de l’élément (pour éviter de devoir mettre des `!important;`).

```CSS
.recit .pull-out{
  font-size: 15pt;
}
```

Pour viser uniquement le n<sup>ième</sup> élément d'un type on utilise `nth-of-type`.

```CSS
.recit .pull-out:nth-of-type(n){
  margin-top: 6pt;
}
```

Sinon on peu inspecter l'élément, afin de voir quelle classe applique le style qu'on veut changer et de copier cette classe.

```CSS
.enquete .pull-out > div > p{
  font-size: 18pt;
}
```

### Variables de couleurs

Si une couleur se répète à plusieurs reprises, cela peut être pratique d'en faire une variable pour pouvoir la changer sur plusieurs éléments d'un coup.

```CSS
@acc_color: midnightblue;

.headline{
	color: @acc_color;
}
.enquete .pull-out{
	color: @acc_color;
}
```

La feuille de style `colors.less` contient **déjà des variables de couleurs propre à l'identité de Médor** à utiliser directement.

### Ajouter une font

Ajouter les fichiers de font est préférable **dans le dossier de l'article**, et non dans le dossier fonts, afin de garder séparé les fonts de la maquette Médor et celle de customisation d'article et d'éviter une accumulation.
Cela permet également de mieux voir les fonts ajoutées sur un numéro.

```CSS
@font-face {
	font-family: "Garamondt";
	src: url("Garamondt-Regular.otf");
	font-weight: normal;
	font-style: normal;
}
```

### Lettrine

Un exemple de lettrine.
Noter que pas beaucoup de propriétés fonctionne sur ce pseudo-élément, par exemple ```position:relative``` ne fonctionne pas.
```CSS
.enquete .body > p:nth-of-type(1)::first-letter{
	@lettrine-height: 3;
	font-family: 'Erbarre';
	font-weight: bold;
	font-size: @lettrine-height * @line-height *(6/5);		/* to adjust according to the font */
	line-height: @lettrine-height * @line-height *(6/5);	/* to adjust according to the font */
	height: @lettrine-height * @line-height;
	float: left;
	padding-right: .1em;
}
```

### Forcer l'indentation d'un paragraphe

Si les paragraphes ne s'indentent pas comme désiré on peut les forcer.
Colorer le paragraphe aide à visualiser ou il se trouve dans l'article.

```CSS
p:nth-of-type(7){
	text-indent:  12pt;
	/* ou */
	text-indent: 0pt;
	color:red;
}
```

### Modifier/Enlever les titres courants ou les numéros de pages

Les titres courant sont des pseudo-éléments ```:before``` de ```.main```, et les numéros de page sont en ```:after```

```CSS
.sheet:nth-child(odd) .main:before {
	/* tout les titres courants */
	content: "Nouveau titre courant" !important;
}
.sheet:nth-of-type(1) .main:after{  
	/* numeros de page sur la 1ere page*/
	color: beige !important;
}
```

Pour les **enlever complètement** les deux sur un page, on utilise la fonction `no-footer()` définie dans `mixins.less`.

```CSS
.no-footer(1);
.no-footer(2);
```


### Exergues

HTML structure

```HTML
<aside class="pull-out" style="">
	<blockquote>
		<div>
			<p>Texte de l'exergue</p>
		</div>
		<footer>
			<p>Crédits</p>
		</footer>
	</blockquote>
</aside>
```

#### Calage sur la hauteur de ligne

Comme les exergues prennent une place vertical variables ils ont tendance à décaler le texte de la grille.

Le script `pullout.js` tente de recalculer l'espace qu'il faudrait ajouter pour que le texte se réajuste sur la grille, et l'ajoute ci-nécéssaire à `aside` en `margin-bottom` en inline style. Le script est imprécis (à cause de l'imprécision de la conversion pixel point ?) et ne recale pas toujours bien le texte.

Pour ajuster ces marges il faut le faire exergues par exergues.

```CSS
.enquete .pull-out:nth-of-type(3){
	margin-top: 14pt;
	margin-bottom: 18pt !important;
}
```

#### Sortir et Rentrer les exergues de la colonne de gauche

Une fonction de ```mixins.less``` permet de facilement sortir le n<sup>ième</sup> exergue sur la colone vide à sa gauche, ou de le rentrer s'il déborde sur une colone remplie.

```CSS
.pull-out-in(3); /*empêche le 3ieme exergue de déborder)*/
.pull-out-out(5); /*fait sortir le 5ieme exergue sur la colonne de gauche)*/
```

#### Cacher des exergues

Ils est possible qu'on préfère garder trop d'exergues dans le wagtail avant de composer, afin de pouvoir tester ceux que l'on met ou non. Le problème de mettre `display: none`, sur un exergue est que le paragraphe suivant ne sera plus indenté car il suit toujours un exergue (et non un autre paragraphe), même si celui-ci ne ce voit plus.
On utilise la `mixins` fonction suivante:

```CSS
.hide-pull-out(3); /*cache le 3ieme exergue en gardant l'indentation du paragraphe suivant*/
```

#### Sortir les exergues du flow

En rajoutant ceci au début du `style.less` d'un article, on sort les exergues dans leur flow individuel

```CSS
// EXERGUES
.pull-out-loop (@i) when (@i > 0) {
	#flow-main blockquote.pull-out:nth-of-type(@{i}) { .flow-into(~"flow-pull-out@{i}"); }
	.flow-pull-out@{i} { .flow-from(~"flow-pull-out@{i}"); }
	.pull-out-loop(@i - 1);
}
.pull-out-loop(3);
```

Ils peuvent être introduit séparément dans l'html en utilisant alors les classes `.flow-pull-out[n]` ou `[n]` est le numéro de l'exergue.


### Images

Les images viennent dans un flow séparé, et peuvent-être positionnées individuellement.

```CSS
.fig-loop (@i) when (@i > 0) {
  #flow-main figure:nth-of-type(@{i}) { .flow-into(~"flow-fig@{i}"); }
  .flow-fig@{i} { .flow-from(~"flow-fig@{i}"); }
  .fig-loop(@i - 1);
}
.fig-loop(11);
```

On positionne l'image avec sa classe de flow correspondante.

```HTML
<div class="bloc t-1 l-1 r-1 b-1 flow-fig2"></div>
```

Par défaut on obtient une structure HTML comme ceci pour les images:
```HTML
<figure>
  <img src="https://meLancerdor.coop/media/images/example.original.jpg">
  <figcaption>
    <footer>Crédit
      <small>License</small>
    </footer>
  </figcaption>
</figure>
```

### Mode de positionnement

Par défaut les images sont en `object-fit: cover`, ce qui agrandis l'image pour couvrir l’entièreté du bloc tout en maintenant son aspect-ratio, en coupant le reste.

Pour ne pas que l'image se retrouve coupée par son container, on peut la passer en `object-fit: contain`, ce qui la force à être contenue dans le bloc.

```CSS
/* la troisième image en contain */
figure:nth-of-type(3) img{
	object-fit: contain;
}
```

Il est préférable de préparer l'image affin qu'elle soit le plus proche possible de son format dans la mise en page. On lui donne sa position et dimension avec les classes de la grille.

```HTML
<div class="bloc x1 w5 t-1 h6 flow-fig2"></div>
```

Pour la bouger plus finement par rapport à sa conteneur on utilise `object-position`, ce qui permet de changer l’alignement du contenu d'une image avec sa boite contenante `img`, on peut ainsi changer l'endroit ou elle est coupée, ou par rapport à quel bords elle est positionnée.

```CSS
figure:nth-of-type(3) > img{
	object-position: center top;
	/* ou */
	object-position: 12pt -96pt;
}
```

#### Images en fond

Pour les image de fond, qui remplisse toute une zone de la page, on peut les ajouter en tant que `background-image` (et non une balise `<img>`) en reprenant son url entier dans le Wagtail.

De cette manière on peut utiliser `background-position` et `background-size`, ce qui est parfois plus pratique.

```HTML
<div class="bloc t-1 b-1 l-1 r-1 fond1"></div>
```

```CSS
.fond{
  background-image: url('https://medor.coop/media/images/example.original.jpg');
  background-repeat: no-repeat;
  z-index: 0;
  background-size: 100%;
  background-position: center center;
}
```

#### Image en fond sur deux pages

La fonction `double();` de `mixins.less` permet de facilement **diviser une grande image sur deux pages**. 
Pour ce faire, on utilise deux fois la même classe, sur deux pages différentes.

```HTML
<!-- PAGES 1 -->
<div class="sheet">
	<div class="page">
		<div class="main">
			<div class="bloc r-1 t-1 l-1 b-1 fond" ></div>
		</div>
	</div>
</div>
<!-- PAGES 2 -->
<div class="sheet">
	<div class="page" >
		<div class="main">
			<div class="bloc r-1 t-1 l-1 b-1 fond" ></div>
		</div>
	</div>
</div>
```

```CSS
.fond{
	background-image: url('https://medor.coop/media/images/portfolioweb-1.original.jpg');
	background-size: auto 100%;
}
.double(".fond", 0cm, 0cm, 1, 2);
```

Le premier paramètre est le nom de la classe qu'on veut mettre en doube, ensuite les décalages par rapport au bord top et left (0 et 0 veut dire que son top-left corner de l'image coïncide avec le top-left corner de la page), et finalement les deux numéro de page sur lesquels se trouvent la double.
Les 4 derniers paramètres ne sont pas obligé d'être précisé.
On peut changer la valeur de `background-size` dans l'unité qu'on veut, et les deux parties resteront collées comme il faut.

Pour que l'image prenne toute la hauteur:
```CSS
.fond{
  background-size: auto 100%;
}
```

<!-- On peu aussi le faire sans utiliser la fonction comme ceci:
```CSS
.fond1, .fond2 {
  background-image: url('https://medor.coop/media/images/example.original.jpg');
  background-repeat: no-repeat;
  background-size: 225%;
  z-index: 0;
}
.fond2 {
  background-position: -@page-width 0;
}
```
Cela déplace vers la gauche (d’où le signe moins) l'image sur la seconde page de la taille de l'intérieure d'une page, `482pt` en récupérant la variable `@page-width`. -->

<!-- Note: percent values must be avoided in this case because
0% -> top d'image = top page
100% -> bas image = bas page
donc le % n'est pas directement par rapport a la taille de l'image,
ni directement par rapport a la taille de la page
mais par rapport au "décalage" entre les deux (pas pratique!) -->

### Notes de bas de page

Les notes de bas de page viennent dans un flow séparé, et peuvent-être positionnées individuellement.

```CSS
.fn-loop (@i) when (@i > 0) {
    #flow-main .footnotes li:nth-of-type(@{i}) { .flow-into(~"flow-fn@{i}"); }
    .flow-fn@{i} { .flow-from(~"flow-fn@{i}"); }
    .fn-loop(@i - 1);
}
.fn-loop(5);
```

```HTML
<div class="bloc x0 y11 w3 h1 flow-fn1"></div>
```

### Mot de fin de paragraphe coupé & word-spacing

Dans le cas ou le dernier mot d'un paragraphe est coupé et saute à la ligne, on peut diminuer légèrement le word spacing de ce paragraphe afin de faire remonter la syllabe à la ligne précédente. Un script `p-nthoftype.js` a été ajouter dans chaque layout (commenter en bas de l'`index.html`, décommenter pour l'utiliser), qui permet en cliquant sur un paragraphe de directement avoir le  `CSS` pour le selectionner. Il sagit d'être prudent car un `word-spacing` négatif trop grand rend le texte illisible, on se limite à `0.05em` sur du texte classique et `0.03em` sur de l'italic (déjà très serré).

---

## toolkit.js

Chaque fichier de layout HTML contient un petit bloc javascript à la fin, ou l'on peut appeller une série de fonctions prédéfinie avec des paramètres variables pour aller plus loins dans la customisation de la mise en page.

Ces fonctions sont définies dans `js/toolkit.js`.

```javascript
document.addEventListener("OnStoryReady", function(){
	//appeller les fonctions ici
});
```

### Scinder un titre sur plusieurs lignes

Pour scinder un titre sur plusieurs lignes il vaut mieux éviter d'ajouter ```<br/>``` dans le Wagtail car:
* il serait visible sous forme de texte brut dans les titres courants, en bas de page.
* il sera appliqué dans la mise en page web de l'article.

Une solution simple est de changer la largeur du container pour faire passer à la ligne, cependant ça n'est pas toujours applicable.

Une fonction du `toolkit.js` peut-être utilisé. qui permet d'ajouter un `<br/>` après le n<sup>ième</sup> mot.
Il faut s'assurer que le toolkit est inclu dans le fichier HTML.

```javascript
document.addEventListener("OnStoryReady", function(){
	add_br_at(2);
});
```

### `replace()` toolkit.js

Permet de customiser plus finement et de spécifier de l'HTML en provenance de l'API wagtail.

```js
document.addEventListener("OnStoryReady", function(){
	// met un <span> autour de chaque "Étape 1" ou "Étape 2", etc
	// affin de pouvoir le selectionner en CSS pour leur appliquer des styles particuliers
	replace("h2:not(.subhead)","Étape ([0-9]).","<span>Étape $1.</span>");
}
```

### Multiple Lettrines

Certain articles commencent par des guillemets, dans ce cas le guillemets et la première lettre deviennent des lettrines.
Une fonction du `toolkit.js` peut-être utilisé, pour ajouter la classe `first-letter` au n premiers caractères.

```javascript
document.addEventListener("OnStoryReady", function(){
  multiple_first_letters(2);
});
```

On peut viser chacun de ces caractères individuellement en css pour une lettrine multiple.
```CSS
.first-letter {
  /* lettrine */
}
.first-letter:nth-of-type(1) {
	/* guillemet en plus petit */
	font-size: 0.5em;
}
.first-letter:nth-of-type(2) {
	margin-left: -10pt;
}
```

--- 

## Le Sommaire

Le sommaire se génère automatiquement sur base du fichier `toc.yaml`.
1. Les journalistes complètent le fichier `toc.yaml`
2. utiliser `make toc` pour générer `build/toc.html`
3. copier-coller le contenu html générer de `build/toc.html` dans le layout `layout/sommaire.html`

On peut ensuite placer les cases en cliquant dessus, puis `ctrl+flèches` pour les bouger et `ctrl+shift+flèches` pour changer leurs tailles.
Pour sauver le résultat, `ctrl+e` permet de copier coller l'html via la textarea en haut, dans le fichier `layout/sommaire.html`.

Les images, couleurs et autres ajustement se font dans le `style.less`.

## Sortir des .pdfs de l'OSPKit

Aller à `http://localhost:8000/layout`

pour passer en 72 dpi:
`xrandr --dpi 72`

Et sélectionner l'article à imprimer, puis le bouton **print** en haut à droite.
Il faut rentrer les dimensions de la page en *custom*, en changeant l'unité en *pt*.
* en 72DPI: ```578pt``` et ```748pt```
* en 96DPI: ```463pt``` et ```600pt```

Ces valeurs sont calculées comme ```@page-width + 2*(@crop-size + @bleed)```

Note: il est nécéssaire de se mettre en 72 dpi pour la sortie finale pour l'impression, sinon les dimensions des pdf ne seront pas les bonnes.

## Pre-printing process

Voir [rgb2cmyk](https://gitlab.constantvzw.org/osp/tools.rgb2cmyk).
